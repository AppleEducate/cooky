import 'package:cooky/cooky.dart' as cookie;

main() async {
  // Create a session cookie:
  cookie.set('key', 'value');

  // Create a cookie that expires in a week:
  cookie.set('key', 'value', maxAge: new Duration(days: 7));

  // Create a cookie that is valid for a certain path and domain:
  cookie.set('key', 'value', path: '/test', domain: 'test.com');

  // Read a cookie:
  var value = cookie.get('key');
  print(value);

  // Delete a cookie:
  cookie.remove('key');
}
