import 'dart:html';

@TestOn('browser')
import 'package:test/test.dart';

import 'package:cooky/cooky.dart';

main() {
  group('cookie', () {
    final unicode = r'ü()<>@,;:\"/[]?={}+-';
    final encoded = Uri.encodeComponent(unicode);

    group('set()', () {
      test('should be able to store a value', () async {
        set('test', 'min');
        expect(document.cookie.contains('test=min'), isTrue);
      });

      test('should be able to set store a value with all arguments', () async {
        set('test', 'max',
            maxAge: new Duration(days: 5), path: 'test', domain: 'localhost');
        expect(document.cookie.contains('test=max'), isTrue);
      });

      test('should throw InvalidArgument exception if key or value are null',
          () async {
        expect(() => set('test', null), throwsArgumentError);
        expect(() => set(null, 'test'), throwsArgumentError);
      });

      test('should be able to store all keys and values', () async {
        set(unicode, unicode);
        expect(document.cookie.contains('$encoded=$encoded'), isTrue);
      });
    });

    group('get()', () {
      test('should retrieve values', () async {
        set('test', 'bar');
        set(unicode, unicode);
        expect(get('test'), 'bar');
        expect(get(unicode), unicode);
      });
    });

    group('remove()', () {
      test('should remove the value from the cookie', () async {
        set('test', 'bar');
        expect(get('test'), 'bar');
        remove('test');
        expect(get('test'), isNull);
      });
    });
  });
}
