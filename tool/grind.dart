library cooky.tool.grind;

import 'dart:io' hide ProcessException;
import 'dart:mirrors';

import 'package:grind_publish/grind_publish.dart' as grind_publish;
import 'package:grinder/grinder.dart';

final Directory projectRoot = new Directory.fromUri(
        currentMirrorSystem().findLibrary(#cooky.tool.grind).uri)
    .parent
    .parent
    .absolute;

@Task('Runs dartanalyzer and fails if there is a hint, warning or lint error')
analyze() async {
  await runAsync('dartanalyzer',
      arguments: ['.', '--fatal-hints', '--fatal-warnings', '--fatal-lints']);
}

@Task()
checkFormat() {
  if (DartFmt.dryRun(projectRoot))
    fail('Code is not properly formatted. Run `grind format`');
}

@Task()
format() => DartFmt.format(projectRoot);

@Task()
testUnit() => new TestRunner().testAsync(
    files: new Directory('test'), platformSelector: ['chrome', 'firefox']);

@Task()
@Depends(checkFormat, analyze, testUnit)
test() => true;

@Task('Automatically publishes this package if the pubspec version increases')
autoPublish() async {
  final credentials = grind_publish.Credentials.fromEnvironment();
  grind_publish.autoPublish('cooky', credentials);
}

/// Setup grinder and logging.
main(List<String> args) {
  grind(args);
}
