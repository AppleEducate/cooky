# Changelog

## 1.0.2

- Updating intl

## 1.0.1

- Minor change: Use `DateTime.now().add(maxAge)` instead of building
  the date with `DateTime.fromMillisecondsSinceEpoch(/* etc... */)`

## 1.0.0

- Initial commit
